from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView
from django.views.generic.base import TemplateView
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

apps_with_api = ("exams",)


# pylint: disable=redefined-builtin
@api_view(["GET"])
def api_root(request, format=None):
    data = {}
    for app in apps_with_api:
        data[app] = reverse(f"{app}-api-root", request=request, format=format)
    return Response(data)


def crash(request):
    _ = 1 / 0


urlpatterns = [
    path(
        "admin/login/",
        RedirectView.as_view(
            url="/accounts/login/epita/",
            permanent=True,
            query_string=True,
        ),
    ),
    path("admin/", admin.site.urls),
    path("accounts/", include("django.contrib.auth.urls")),
    path("accounts/", include("social_django.urls", namespace="social")),
    path("", TemplateView.as_view(template_name="exams/index.html"), name="index"),
    path("api/", api_root),
    path("api/crash/", crash),
    path("api/doc/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "api/doc/swagger/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="schema-swagger",
    ),
    path(
        "api/doc/redoc/",
        SpectacularRedocView.as_view(url_name="schema"),
        name="schema-redoc",
    ),
] + [path(f"api/{app}/", include(f"{app}.urls")) for app in apps_with_api]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
