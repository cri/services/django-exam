from django.conf import settings


class KerberosPrincipalStr(str):
    @property
    def primary(self):
        return self._primary

    @property
    def instance(self):
        return self._instance

    @property
    def realm(self):
        return self._realm

    # str is immutable and can't have an __init__ method.
    def __new__(
        cls, principal=None, *, primary=None, instance=None, realm=None, **kwargs
    ):
        (primary, instance, realm) = analyze_principal(
            principal, primary=primary, instance=instance, realm=realm
        )
        if instance:
            principal = f"{primary}/{instance}@{realm}"
        else:
            principal = f"{primary}@{realm}"
        obj = str.__new__(cls, principal, **kwargs)
        obj._primary = primary
        obj._instance = instance
        obj._realm = realm
        return obj

    def __repr__(self):
        return f"<KerberosPrincipalStr: {self}>"


class MalformedPrincipal(ValueError):
    pass


def analyze_principal(principal=None, *, primary=None, instance=None, realm=None):
    final_primary = None
    final_instance = None
    final_realm = None

    if (
        hasattr(principal, "primary")
        and hasattr(principal, "instance")
        and hasattr(principal, "realm")
    ):
        final_primary = principal.primary
        final_instance = principal.instance
        final_realm = principal.realm
    elif principal:
        if "@" not in principal:
            primary_instance = principal
        else:
            primary_instance, final_realm = principal.split("@", 1)
            if "@" in final_realm:
                raise MalformedPrincipal(principal)

        if "/" not in primary_instance:
            final_primary = primary_instance
        else:
            final_primary, final_instance = primary_instance.split("/", 1)
            if "/" in final_instance:
                raise MalformedPrincipal(principal)

    if primary:
        final_primary = primary
    if instance:
        final_instance = instance
    if realm:
        final_realm = realm

    if final_primary is None:
        final_primary = ""

    if not final_realm:
        final_realm = settings.KERBEROS_DEFAULT_REALM

    return (final_primary, final_instance, final_realm)
