from django.core.exceptions import SuspiciousOperation
from django.http import HttpResponse
from django.utils.deprecation import MiddlewareMixin

from .. import gssapi
from .backend import SPNEGOBackend
from .constants import (
    HTTP_AUTHORIZATION,
    NEGOTIATE,
    SPNEGO_REQUEST_STATUS,
    WWW_AUTHENTICATE,
)


class SPNEGOMiddleware(MiddlewareMixin):
    def process_exception(self, request, exception):
        if isinstance(exception, SPNEGOBackend.SPNEGOIncompleteAuth):
            response = HttpResponse("Unauthorized", status=SPNEGO_REQUEST_STATUS)
            response[WWW_AUTHENTICATE] = f"{NEGOTIATE} {exception.auth_data}"
            return response
        return None

    def process_request(self, request):
        request.spnego_user = None
        request.spnego_error = None
        authorization_headers = request.headers.get(HTTP_AUTHORIZATION, "").split(",")
        for authorization in authorization_headers:
            auth_tuple = authorization.split(" ", 1)
            if not auth_tuple or auth_tuple[0] != NEGOTIATE:
                continue
            if len(auth_tuple) != 2:
                raise SuspiciousOperation("Malformed authorization header")
            try:
                principal = SPNEGOBackend().gssapi_auth(request, auth_tuple[1])
                if principal:
                    del request.META["HTTP_AUTHORIZATION"]
                    request.META["REMOTE_USER"] = principal.primary
            except gssapi.GSSAuthError as e:
                request.spnego_error = e

    def process_response(self, request, response):
        if request.spnego_user:
            response[WWW_AUTHENTICATE] = f"{NEGOTIATE} {request.spnego_user.auth_data}"
        return response
