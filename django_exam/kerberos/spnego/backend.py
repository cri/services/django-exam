import logging

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.core.exceptions import SuspiciousOperation

from ..gssapi import KerberosGSSAuthServer
from ..utils import KerberosPrincipalStr
from .constants import HTTP_AUTHORIZATION, NEGOTIATE

_logger = logging.getLogger(__name__)


class AuthBackendMixin:
    def get_user(self, user_id):
        User = get_user_model()
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            _logger.error("user %s does not exist anymore", user_id)
            return None


class SPNEGOBackend(AuthBackendMixin, ModelBackend):
    class SPNEGOIncompleteAuth(Exception):
        # pylint:disable=unused-private-member
        def __init_(self, auth_data):
            self.auth_data = auth_data

    def __init__(self, *args, **kwargs):
        self.logger = logging.getLogger(__name__)
        super().__init__(*args, **kwargs)

    def user_from_principal(self, kerberos_principal):
        self.logger.debug("Looking up user with principal '%s'", kerberos_principal)
        krb_principal_data = KerberosPrincipalStr(kerberos_principal)
        try:
            user = get_user_model().objects.get(username=krb_principal_data.primary)
        except get_user_model().DoesNotExist:
            self.logger.debug("User with principal '%s' not found", kerberos_principal)
            return None

        self.logger.debug("Found user '%s'", user)
        return user

    def gssapi_auth(self, request, authstr):
        service = f"HTTP@{settings.SPNEGO_HOSTNAME}"
        self.logger.debug("using service name %s", service)
        self.logger.debug("Negotiate authstr %r", authstr)

        with KerberosGSSAuthServer(service) as krbgssauth:
            result, response = krbgssauth.step(authstr)
            if result == KerberosGSSAuthServer.AUTH_GSS_CONTINUE:
                raise SPNEGOBackend.SPNEGOIncompleteAuth(auth_data=response)
            return KerberosPrincipalStr(krbgssauth.get_username())

    def authenticate(self, request):  # pylint:disable=arguments-differ
        authorization_headers = request.headers.get(HTTP_AUTHORIZATION, "").split(",")

        for authorization in authorization_headers:
            auth_tuple = authorization.split(" ", 1)
            if not auth_tuple or auth_tuple[0] != NEGOTIATE:
                continue
            if len(auth_tuple) != 2:
                raise SuspiciousOperation("Malformed authorization header")
            principal = self.gssapi_auth(request, auth_tuple[1])
            user = self.user_from_principal(principal)
            if user:
                user.auth_data = response
                user.auth_principal = principal
                return user
        return None
