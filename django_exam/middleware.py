import base64
import binascii
import ipaddress

import requests
from django.conf import settings
from django.core.exceptions import SuspiciousOperation
from django.db import Error as DBError
from django.db import connections
from django.http import HttpResponse
from django.utils.deprecation import MiddlewareMixin


class XRealIPMiddleware(MiddlewareMixin):
    REAL_IP_HEADER = "HTTP_X_REAL_IP"

    def process_request(self, request):
        try:
            real_ip = ipaddress.ip_address(request.META.get(self.REAL_IP_HEADER))
        except ValueError:
            return
        request.META["REMOTE_ADDR"] = str(real_ip)


class ProbesMiddleware(MiddlewareMixin):
    paths = {"/readiness": "readiness", "/healthz": "healthz"}

    def is_method_allowed(self, request):
        return request.method == "GET"

    def is_ip_allowed(self, request):
        ip = ipaddress.ip_address(request.META.get("REMOTE_ADDR"))
        for network in map(ipaddress.ip_network, settings.PROBES_IPS):
            if ip in network:
                return True
        return False

    def process_request(self, request):
        if not self.is_method_allowed(request) or not self.is_ip_allowed(request):
            return None

        handler = self.paths.get(request.path, "_default_handler")
        return getattr(self, handler)(request)

    def _default_handler(self, _request):
        return None

    def readiness(self, request):
        for db_alias in connections.databases:
            if not self.check_database(db_alias):
                return HttpResponse(
                    f"error: unable to query database: {db_alias}", status=503
                )
        return self.healthz(request)

    def healthz(self, _request):
        return HttpResponse("ok")

    def check_database(self, db_alias):
        db_settings = connections.databases.get(db_alias, {})
        try:
            if db_settings.get("ENGINE", "").startswith("django.db.backends."):
                connections[db_alias].cursor().execute("SELECT 1")
        except DBError:
            return False
        return True


class CRIBasicAuthAuthenificationMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        authorization_header = request.META.get("HTTP_AUTHORIZATION", None)
        request.META.pop("REMOTE_USER", None)
        if authorization_header:
            username, password = self.extract_basicauth(authorization_header)
            if username and self.validate_creds(username, password):
                del request.META["HTTP_AUTHORIZATION"]
                request.META["REMOTE_USER"] = username
        return self.get_response(request)

    def validate_creds(self, username, password):
        return (
            requests.get(
                "https://cri.epita.fr/api/v2/users/me/",
                auth=(username, password),
            ).status_code
            == 200
        )

    def extract_basicauth(self, authorization_header, encoding="latin-1"):
        splitted = authorization_header.split(" ")
        if len(splitted) != 2:
            return None

        auth_type, auth_string = splitted

        if "basic" != auth_type.lower():
            return None, None

        try:
            b64_decoded = base64.b64decode(auth_string)
        except (TypeError, binascii.Error):
            return None, None
        try:
            auth_string_decoded = b64_decoded.decode(encoding)
        except UnicodeDecodeError as e:
            raise SuspiciousOperation from e

        splitted = auth_string_decoded.split(":", 1)

        if len(splitted) != 2:
            raise SuspiciousOperation

        return splitted
