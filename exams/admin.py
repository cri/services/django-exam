from django.contrib import admin
from django.db.models import Q

from .models import Exam, ExamFile, ExamGroup, Network, StartTime, SubmissionShift


def get_user_exams(user):
    if user.is_superuser:
        return Exam.objects.all()
    return Exam.objects.filter(
        Q(exam_group__admin_users=user) | Q(exam_group__admin_groups__user=user),
    ).distinct()


@admin.register(ExamGroup)
class ExamGroupAdmin(admin.ModelAdmin):
    list_display = ("name", "description")
    list_display_links = list_display
    search_fields = ("name", "description")
    filter_horizontal = ("admin_groups", "admin_users")


class ExamFileInline(admin.StackedInline):
    model = ExamFile

    def __init__(self, *args, readonly=False, **kwargs):
        if readonly:
            self.extra = 0
            self.max_num = 0
            self.can_delete = False
        super().__init__(*args, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.archived:
            return [
                f.name
                for f in self.model._meta.get_fields()
                if not getattr(f, "multiple", False)
            ]
        return self.readonly_fields


@admin.register(Exam)
class ExamAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "token",
        "begin",
        "end",
        "duration",
        "exam_group",
        "closed",
        "archived",
    )
    list_display_links = list_display
    list_filter = (
        "archived",
        "closed",
        "exam_group",
        "networks",
        "allow_submission",
    )
    date_hierarchy = "begin"
    search_fields = (
        "token",
        "title",
        "exam_group__name",
        "begin",
        "end",
        "networks__name",
        "created_by__username",
    )
    readonly_fields = (
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
        "zipfile",
    )
    filter_horizontal = ("networks",)
    inlines = (ExamFileInline,)
    save_as = True

    def get_queryset(self, request):
        return get_user_exams(request.user)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "exam_group":
            # Only show exam group for which the user is an admin
            if not request.user.is_superuser:
                kwargs["queryset"] = ExamGroup.objects.filter(
                    Q(admin_users=request.user) | Q(admin_groups__user=request.user),
                ).distinct()
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_inline_instances(self, request, obj=None):
        args = (self.model, self.admin_site)
        kwargs = {}
        if obj and obj.archived:
            kwargs["readonly"] = True
        return [inline(*args, **kwargs) for inline in self.inlines]

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.archived:
            return [
                f.name
                for f in type(obj)._meta.get_fields()
                if f.name != "archived" and not getattr(f, "multiple", False)
            ]
        return self.readonly_fields

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        obj.updated_by = request.user
        super().save_model(request, obj, form, change)


@admin.register(Network)
class NetworkAdmin(admin.ModelAdmin):
    list_display = ("name", "subnet")
    list_display_links = list_display
    search_fields = ("name", "subnet")


@admin.register(StartTime)
class StartTimeAdmin(admin.ModelAdmin):
    list_display = (
        "exam",
        "username",
        "start_time",
    )
    list_display_links = list_display
    list_filter = (
        "exam__archived",
        "exam__token",
    )
    search_fields = ("exam__title", "exam__token", "username")
    date_hierarchy = "start_time"
    readonly_fields = ("start_time",)
    autocomplete_fields = ("exam",)

    def get_queryset(self, request):
        return StartTime.objects.filter(
            exam__in=get_user_exams(request.user),
        )


@admin.register(SubmissionShift)
class SubmissionShiftAdmin(admin.ModelAdmin):
    list_display = (
        "exam",
        "username",
        "delay",
    )
    list_display_links = list_display
    list_filter = (
        "exam__archived",
        "exam__token",
    )
    search_fields = ("exam__title", "exam__token", "username")

    def get_queryset(self, request):
        return SubmissionShift.objects.filter(
            exam__in=get_user_exams(request.user),
        )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "exam":
            # Only show exams for which the user is an admin
            if not request.user.is_superuser:
                kwargs["queryset"] = get_user_exams(request.user)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)
