from django.urls import include, path
from rest_framework_extensions.routers import ExtendedDefaultRouter

from .views import ExamViewSet

router = ExtendedDefaultRouter()
router.root_view_name = "exams-api-root"
exam_router = router.register("", ExamViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
