import ipaddress

from django.utils import timezone
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .models import Exam, StartTime
from .serializers import ExamSerializer, RemainingTimeSerializer


class ExamViewSet(GenericViewSet):
    queryset = Exam.objects.all()
    serializer_class = ExamSerializer
    lookup_field = "token"

    def initial_checks(self, request, source_ip, *args, **kwargs):
        exam = self.get_object()

        messages = []
        if exam.closed:
            messages.append("exam is closed")
        if exam.begin > timezone.now():
            messages.append("exam has not begun")
        if exam.end < timezone.now():
            messages.append("exam has ended")
        if not any(source_ip in n.subnet for n in exam.networks.all()):
            messages.append("your IP address is not allowed")

        if not request.user.is_superuser and messages:
            raise PermissionDenied({"errors": "\n".join(messages)})

    @action(methods=["post"], detail=True, permission_classes=[IsAuthenticated])
    def start(self, request, *args, **kwargs):
        exam = self.get_object()

        source_ip = ipaddress.ip_address(request.META.get("REMOTE_ADDR"))
        self.initial_checks(request, source_ip, *args, **kwargs)

        StartTime.objects.get_or_create(username=request.user.username, exam=exam)

        serializer = self.get_serializer_class()(exam)
        return Response(serializer.data)

    @action(methods=["post"], detail=True, permission_classes=[AllowAny])
    def remaining_time(self, request, *args, **kwargs):
        exam = self.get_object()

        serializer = RemainingTimeSerializer(data=request.data)
        if not serializer.is_valid():
            raise ValueError("Invalid request.")

        source_ip = ipaddress.ip_address(serializer.validated_data["source_ip"])
        self.initial_checks(request, source_ip, *args, **kwargs)

        remaining_time = int(
            exam.get_remaining_time(
                serializer.validated_data["username"]
            ).total_seconds()
        )

        if remaining_time == 0:
            raise PermissionDenied("No remaining time.")
        return Response({"remaining_time": remaining_time})
