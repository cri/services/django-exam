from rest_framework import serializers

from .models import Exam, Network


class NetworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Network
        fields = ("name", "subnet")


class ExamSerializer(serializers.ModelSerializer):
    created_at = serializers.CharField(read_only=True)
    created_by = serializers.CharField(read_only=True)
    updated_at = serializers.CharField(read_only=True)
    updated_by = serializers.CharField(read_only=True)
    networks = NetworkSerializer(many=True, read_only=True)

    class Meta:
        model = Exam
        fields = (
            "token",
            "title",
            "exam_group",
            "closed",
            "begin",
            "end",
            "duration",
            "networks",
            "allow_submission",
            "created_at",
            "created_by",
            "updated_at",
            "updated_by",
            "zipfile",
        )


class RemainingTimeSerializer(serializers.Serializer):
    source_ip = serializers.IPAddressField()
    username = serializers.CharField()
