import io
import os
import zipfile
from datetime import timedelta

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.db import models
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from django.shortcuts import get_object_or_404
from django.utils import timezone
from netfields import CidrAddressField

from django_exam.storage_backends import PrivateMediaStorage


class Network(models.Model):
    name = models.CharField(max_length=200, unique=True)

    subnet = CidrAddressField()

    class Meta:
        ordering = ("subnet",)

    def __str__(self):
        return f"{self.name} ({str(self.subnet)})"


class ExamGroup(models.Model):
    name = models.CharField(max_length=200, unique=True)

    description = models.TextField(blank=True)

    admin_groups = models.ManyToManyField(Group, blank=True)

    admin_users = models.ManyToManyField(get_user_model(), blank=True)

    def __str__(self):
        return self.name


class Exam(models.Model):
    token = models.SlugField(
        unique=True,
        help_text="Keep it short and understandable, students will have to type it!",
    )

    title = models.CharField(
        max_length=200,
        help_text="A name to keep track of your exams.",
    )

    exam_group = models.ForeignKey(
        ExamGroup,
        on_delete=models.PROTECT,
    )

    closed = models.BooleanField(
        default=False,
        help_text="Check this box to forbid any submission.",
    )

    begin = models.DateTimeField()

    end = models.DateTimeField()

    duration = models.DurationField(default=timedelta)

    networks = models.ManyToManyField(Network, blank=False)

    allow_submission = models.BooleanField(
        default=False,
        help_text="Check this box to allow student to use the `submission` command.",
    )

    archived = models.BooleanField(
        default=False,
        help_text="Check this box to hide this exam.",
    )

    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    created_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_NULL,
        related_name="exam_created",
        editable=False,
        blank=True,
        null=True,
    )

    updated_at = models.DateTimeField(auto_now=True, editable=False)
    updated_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_NULL,
        related_name="exam_updated",
        editable=False,
        blank=True,
        null=True,
    )

    zipfile = models.FileField(storage=PrivateMediaStorage(), blank=True, null=True)

    class Meta:
        ordering = ("exam_group", "title")
        unique_together = (("title", "exam_group"),)

    def clean(self):
        errors = {}

        if self.begin >= self.end:
            v = ValidationError("Begin date must be before end date")
            errors.setdefault("begin", []).append(v)
            errors.setdefault("end", []).append(v)

        if self.begin + self.duration > self.end:
            v = ValidationError("Begin date + duration must be <= to end date")
            errors.setdefault("begin", []).append(v)
            errors.setdefault("end", []).append(v)
            errors.setdefault("duration", []).append(v)

        if self.archived and not (self.closed or self.end < timezone.now()):
            v = ValidationError("You can only archive closed or ended exams")
            errors.setdefault("closed", []).append(v)
            errors.setdefault("end", []).append(v)
            errors.setdefault("archived", []).append(v)

        if errors:
            raise ValidationError(errors)

    def __str__(self):
        return f"{self.title} ({self.token})"

    def get_remaining_time(self, username):
        if timezone.now() < self.begin:
            raise ValueError("Exam has not started yet.")

        duration = self.duration

        submission_shift = SubmissionShift.objects.filter(
            exam=self,
            username=username,
        ).first()
        if submission_shift:
            duration += submission_shift.delay

        start_time = get_object_or_404(
            StartTime, exam=self, username=username
        ).start_time

        user_end = min(start_time + duration, self.end)
        return max(user_end - timezone.now(), timedelta(seconds=0))

    def build_zipfile(self):
        s = io.BytesIO()
        with zipfile.ZipFile(s, "w") as zf:
            for examfile in ExamFile.objects.filter(exam=self):
                zip_subdir = "skel" if examfile.belongs_to_skel else "subject"
                file = examfile.file

                if examfile.require_unzip:
                    with zipfile.ZipFile(io.BytesIO(file.read())) as fp:
                        for name in fp.namelist():
                            zip_path = os.path.join(zip_subdir, name)
                            zf.writestr(zip_path, fp.read(name))
                else:
                    zip_path = os.path.join(zip_subdir, file.name)
                    zf.writestr(zip_path, file.read())
        self.zipfile.save(f"{self.token}.zip", ContentFile(s.getvalue()))


class ExamFile(models.Model):
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)

    file = models.FileField(storage=PrivateMediaStorage())

    belongs_to_skel = models.BooleanField()

    require_unzip = models.BooleanField()


class StartTime(models.Model):
    username = models.CharField(max_length=200)

    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)

    start_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.exam} - {self.username}"

    class Meta:
        unique_together = ("username", "exam")


class SubmissionShift(models.Model):
    username = models.CharField(max_length=200)

    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)

    delay = models.DurationField(default=timedelta)

    def __str__(self):
        return f"{self.exam} - {self.username}"

    class Meta:
        unique_together = ("username", "exam")


# pylint:disable=unused-argument
@receiver(post_save, sender=ExamFile)
@receiver(post_delete, sender=ExamFile)
def exam_zipfile(sender, instance, **kwargs):
    exam = instance.exam
    exam.build_zipfile()
